<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 10.02.18
 * Time: 16:58
 */

namespace AppBundle\Exception;


use Exception;

class ObjectNotFoundException extends \Exception
{

    public function __construct($message = "", $code = 404, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

}