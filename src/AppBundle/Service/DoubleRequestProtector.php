<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 12.02.18
 * Time: 22:59
 */

namespace AppBundle\Service;


use AppBundle\Repository\RequestRepository;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;

class DoubleRequestProtector
{

    /**
     * @var Serializer
     */
    protected $serializer;

    /**
     * @var RequestRepository
     */
    protected $requestRepository;


    public function __construct(
        SerializerInterface $serializer,
        RequestRepository $requestRepository
    )
    {
        $this->serializer           = $serializer;
        $this->requestRepository    = $requestRepository;
    }


    /**
     * @param Request $request
     * @return bool
     */
    public function requestIsAllow(Request $request)
    {
        $rHash = $this->makeHashRequest($request);

        return !$this->requestRepository->getByHash($rHash);
    }

    /**
     * @param Request $request
     */
    public function markRequest(Request $request)
    {
        $rHash = $this->makeHashRequest($request);

        $this->requestRepository->add($rHash);
    }

    /**
     * @param Request $request
     * @return string
     */
    protected function makeHashRequest(Request $request)
    {
        $serialized = $this->serializer->serialize([
            'query' => $request->query,
            'request' => $request->request,
        ], 'json');

        return md5($request->getUri().$serialized);
    }

}