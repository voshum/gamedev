<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 10.02.18
 * Time: 16:55
 */

namespace AppBundle\Service;


use AppBundle\Entity\Campaign;
use AppBundle\Entity\CampaignType;
use AppBundle\Entity\Repository\CampaignRepositoryInterface;
use AppBundle\Entity\Repository\CampaignTypeRepositoryInterface;
use AppBundle\Entity\Repository\UserRepositoryInterface;
use AppBundle\Entity\User;
use AppBundle\Exception\ObjectNotFoundException;

class CampaignService
{

    /**
     * @var UserRepositoryInterface
     */
    protected $userRepository;

    /**
     * @var CampaignRepositoryInterface
     */
    protected $campaignRepository;

    /**
     * @var CampaignTypeRepositoryInterface
     */
    protected $campaignTypeRepository;

    /**
     * CampaignService constructor.
     * @param UserRepositoryInterface $userRepository
     * @param CampaignRepositoryInterface $campaignRepository
     * @param CampaignTypeRepositoryInterface $campaignTypeRepository
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        CampaignRepositoryInterface $campaignRepository,
        CampaignTypeRepositoryInterface $campaignTypeRepository
    )
    {
        $this->userRepository = $userRepository;
        $this->campaignRepository = $campaignRepository;
        $this->campaignTypeRepository = $campaignTypeRepository;
    }


    /**
     * @param $userId
     * @param $campaignTypeId
     * @param $name
     * @param null $customSetting
     * @return Campaign
     * @throws ObjectNotFoundException
     */
    public function createCampaign($userId, $campaignTypeId, $name, $customSetting = null)
    {
        $user = $this->userRepository->getById($userId);

        if (!$user instanceof User) {
            throw new ObjectNotFoundException(sprintf('User %s not found', $userId));
        }

        $type = $this->campaignTypeRepository->getById($campaignTypeId);

        if (!$type instanceof CampaignType) {
            throw new ObjectNotFoundException(sprintf('Type %s not found', $campaignTypeId));
        }

        $campaign = new Campaign($user, $type, $name, $customSetting);

        $this->campaignRepository->add($campaign);

        return $campaign;
    }

    /**
     * @param $campaignId
     * @param $campaignTypeId
     * @param $name
     * @param $customSetting
     * @return Campaign
     * @throws ObjectNotFoundException
     */
    public function updateCampaign($campaignId, $campaignTypeId, $name, $customSetting)
    {
        $campaign = $this->getCampaign($campaignId);

        $type = $this->campaignTypeRepository->getById($campaignTypeId);

        if (!$type instanceof CampaignType) {
            throw new ObjectNotFoundException(sprintf('Type %s not found', $campaignTypeId));
        }

        $campaign->setName($name);
        $campaign->setCustomSetting($customSetting);
        $campaign->setType($type);

        $this->campaignRepository->update($campaign);

        return $campaign;
    }


    /**
     * @param int $limit
     * @param int $offset
     * @return Campaign[]
     */
    public function getCampaigns($limit = 100, $offset = 0)
    {
        return $this->campaignRepository->getCampaigns($limit, $offset);
    }

    /**
     * @param $id
     * @return Campaign
     * @throws ObjectNotFoundException
     */
    public function getCampaign($id)
    {
        $campaign = $this->campaignRepository->getById($id);

        if (!$campaign instanceof Campaign) {
            throw new ObjectNotFoundException(sprintf('Campaign %s not found', $id));
        }

        return $campaign;
    }

    /**
     * @param $id
     */
    public function deleteCampaign($id)
    {
        $campaign = $this->getCampaign($id);

        $this->campaignRepository->remove($campaign);
    }




}