<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 08.02.18
 * Time: 22:49
 */

namespace AppBundle\Service;


use AppBundle\Entity\Repository\UserRepositoryInterface;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserService
{

    /**
     * @var UserRepositoryInterface
     */
    protected $userRepository;

    /**
     * @var UserPasswordEncoderInterface
     */
    protected $passwordEncoder;


    /**
     * UserService constructor.
     * @param UserRepositoryInterface $userRepository
     * @param UserPasswordEncoderInterface $userPasswordEncoder
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        UserPasswordEncoderInterface $userPasswordEncoder
    )
    {
        $this->userRepository = $userRepository;
        $this->passwordEncoder = $userPasswordEncoder;
    }


    /**
     * @param $name
     * @param $email
     * @param $plainPassword
     * @return User
     */
    public function createUser($name, $email, $plainPassword)
    {
        $user = new User($name, $email);

        $encodedPassword = $this->passwordEncoder->encodePassword($user, $plainPassword);

        $user->setPassword($encodedPassword);
        $user->setSalt('');

        $this->userRepository->add($user);

        return $user;
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return User[]
     */
    public function getUsers($limit = 100, $offset = 0)
    {
        return $this->userRepository->getUsers($limit, $offset);
    }


}