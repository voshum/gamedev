<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 10.02.18
 * Time: 13:40
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Repository\UserRepositoryInterface;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository implements UserRepositoryInterface
{

    /**
     * @param User $user
     */
    public function add(User $user)
    {
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush($user);
    }

    /**
     * @param $limit
     * @param $offset
     * @return User[]
     */
    public function getUsers($limit, $offset)
    {
        return $this->createQueryBuilder('user')
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $id
     * @return User|null
     */
    public function getById($id)
    {
        return $this->find($id);
    }

    /**
     * @param $email
     * @return mixed
     */
    public function getByEmail($email)
    {
        return $this->createQueryBuilder('user')
            ->where('user.email = :EMAIL')
            ->setParameter(':EMAIL', $email)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }


}