<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 12.02.18
 * Time: 22:02
 */

namespace AppBundle\Repository;


use AppBundle\Entity\CampaignType;
use AppBundle\Entity\Repository\CampaignTypeRepositoryInterface;
use AppBundle\Utils\TestUtils\InMemoryRepository;

class InMemoryCampaignTypeRepository extends InMemoryRepository implements CampaignTypeRepositoryInterface
{


    public function add(CampaignType $campaignType)
    {
        $this->storage->add($campaignType);
    }

    public function getById($id)
    {
        return $this->storage->getByPk($id);
    }

}