<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 12.02.18
 * Time: 22:00
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Campaign;
use AppBundle\Entity\Repository\CampaignRepositoryInterface;
use AppBundle\Utils\TestUtils\InMemoryRepository;

class InMemoryCampaignRepository extends InMemoryRepository implements CampaignRepositoryInterface
{

    public function add(Campaign $campaign)
    {
        $this->storage->add($campaign);
    }

    public function getCampaigns($limit, $offset)
    {
        return $this->storage->all();
    }

    public function getById($id)
    {
        return $this->storage->getByPk($id);
    }

    public function remove(Campaign $campaign)
    {
        $this->storage->delete($campaign->getId());
    }

    public function update(Campaign $campaign)
    {

    }


}