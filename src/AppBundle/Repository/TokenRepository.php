<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 11.02.18
 * Time: 15:17
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Repository\TokenRepositoryInterface;
use AppBundle\Entity\Token;
use SymfonyBundles\RedisBundle\Redis\Client;

class TokenRepository implements TokenRepositoryInterface
{

    /**
     * @var Client
     */
    protected $redisClient;

    /**
     * TokenRepository constructor.
     * @param Client $redisClient
     */
    public function __construct(Client $redisClient)
    {
        $this->redisClient = $redisClient;
    }

    /**
     * @param string $apiKey
     * @return Token|null
     */
    public function getByApiKey($apiKey)
    {
        $record = $this->redisClient->get($apiKey);

        if ($record === null) {
            return null;
        }

        $normalizedData = json_decode($record, true);

        if (!isset($normalizedData['username']) || !isset($normalizedData['expired_at'])) {
            return null;
        }

        return new Token(
            $apiKey, $normalizedData['username'], $normalizedData['expired_at']
        );
    }


}