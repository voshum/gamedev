<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 10.02.18
 * Time: 15:36
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Campaign;
use AppBundle\Entity\Repository\CampaignRepositoryInterface;
use Doctrine\ORM\EntityRepository;

class CampaignRepository extends EntityRepository implements CampaignRepositoryInterface
{
    /**
     * @param Campaign $campaign
     */
    public function add(Campaign $campaign)
    {
        $this->getEntityManager()->persist($campaign);
        $this->getEntityManager()->flush($campaign);
    }

    /**
     * @param $limit
     * @param $offset
     * @return array
     */
    public function getCampaigns($limit, $offset)
    {
        return $this->createQueryBuilder('c')
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $id
     * @return null|Campaign
     */
    public function getById($id)
    {
        return $this->find($id);
    }

    /**
     * @param Campaign $campaign
     */
    public function remove(Campaign $campaign)
    {
        $this->getEntityManager()->remove($campaign);
        $this->getEntityManager()->flush($campaign);
    }

    /**
     * @param Campaign $campaign
     */
    public function update(Campaign $campaign)
    {
        $this->getEntityManager()->flush($campaign);
    }


}