<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 10.02.18
 * Time: 15:37
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Campaign;
use AppBundle\Entity\CampaignType;
use AppBundle\Entity\Repository\CampaignRepositoryInterface;
use AppBundle\Entity\Repository\CampaignTypeRepositoryInterface;
use Doctrine\ORM\EntityRepository;

class CampaignTypeRepository extends EntityRepository implements CampaignTypeRepositoryInterface
{

    /**
     * @param CampaignType $campaignType
     */
    public function add(CampaignType $campaignType)
    {
        $this->getEntityManager()->persist($campaignType);
        $this->getEntityManager()->flush($campaignType);
    }

    /**
     * @param $id
     * @return null|CampaignType
     */
    public function getById($id)
    {
        return $this->find($id);
    }


}