<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 10.02.18
 * Time: 12:26
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Repository\UserRepositoryInterface;
use AppBundle\Entity\User;
use AppBundle\Utils\TestUtils\InMemoryRepository;

class InMemoryUserRepository extends InMemoryRepository implements UserRepositoryInterface
{

    /**
     * @param User $user
     */
    public function add(User $user)
    {
        $this->storage->add($user);
    }

    /**
     * @param $limit
     * @param $offset
     * @return User[]
     */
    public function getUsers($limit, $offset)
    {
        return $this->storage->all();
    }

    /**
     * @param $id
     * @return mixed|null
     */
    public function getById($id)
    {
        return $this->storage->getByPk($id);
    }

    public function getByEmail($email)
    {
        /** @var User $item */
        foreach ($this->storage as $item) {
            if ($item->getEmail() === $email) {
                return $item;
            }
        }

        return null;
    }


}