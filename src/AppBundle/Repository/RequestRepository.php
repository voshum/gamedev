<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 12.02.18
 * Time: 23:04
 */

namespace AppBundle\Repository;


use Predis\Client;

class RequestRepository
{

    /**
     * @var Client
     */
    protected $redisClient;

    /**
     * TokenRepository constructor.
     * @param Client $redisClient
     */
    public function __construct(Client $redisClient)
    {
        $this->redisClient = $redisClient;
    }

    /**
     * @param $hash
     */
    public function add($hash)
    {
        $this->redisClient->set($hash, $hash, 'ex', 60);
    }

    /**
     * @param $hash
     */
    public function getByHash($hash)
    {
        return $this->redisClient->get($hash);
    }


}