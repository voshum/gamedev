<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 11.02.18
 * Time: 14:17
 */

namespace AppBundle\Security;


use AppBundle\Entity\Repository\TokenRepositoryInterface;
use AppBundle\Entity\Repository\UserRepositoryInterface;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class ApiKeyUserProvider implements UserProviderInterface
{

    /**
     * @var UserRepositoryInterface
     */
    protected $userRepository;

    /**
     * @var TokenRepositoryInterface
     */
    protected $tokenRepository;

    /**
     * ApiKeyUserProvider constructor.
     * @param UserRepositoryInterface $userRepository
     * @param TokenRepositoryInterface $tokenRepository
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        TokenRepositoryInterface $tokenRepository
    )
    {
        $this->userRepository = $userRepository;
        $this->tokenRepository = $tokenRepository;
    }

    /**
     * @param $apiKey
     * @return \AppBundle\Entity\Token
     */
    public function getTokenByKey($apiKey)
    {
        return $this->tokenRepository->getByApiKey($apiKey);
    }


    /**
     * @param string $username
     * @return User|null
     */
    public function loadUserByUsername($username)
    {
        return $this->userRepository->getByEmail($username);
    }

    /**
     * @param UserInterface $user
     */
    public function refreshUser(UserInterface $user)
    {
        throw new UnsupportedUserException();
    }

    /**
     * @param string $class
     * @return bool
     */
    public function supportsClass($class)
    {
        return User::class === $class;
    }


}