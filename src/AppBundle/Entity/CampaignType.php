<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 08.02.18
 * Time: 22:37
 */

namespace AppBundle\Entity;


use Gedmo\Timestampable\Traits\TimestampableEntity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CampaignTypeRepository")
 * @ORM\Table(name="campaign_type")
 * @Serializer\ExclusionPolicy(value="all")
 */

class CampaignType
{
    use TimestampableEntity;

    const STATUS_ENABLE = 'Enable';

    const STATUS_DISABLE = 'Disable';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose()
     *
     * @var int
     */
    protected $id;

    /**
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Serializer\Expose()
     *
     * @var string
     */
    protected $name;

    /**
     * @ORM\Column(name="status", type="string", length=255, nullable=false)
     * @Serializer\Expose()
     *
     * @var string
     */
    protected $status;

    /**
     * CampaignType constructor.
     * @param string $name
     * @param string $status
     */
    public function __construct($name, $status = self::STATUS_DISABLE)
    {
        $this->name = $name;
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return $this
     */
    public function toEnableStatus()
    {
        if ($this->status == self::STATUS_ENABLE) {
            throw new \LogicException('Status already enable');
        }

        $this->status = self::STATUS_ENABLE;

        return $this;
    }

    /**
     * @return $this
     */
    public function toDisableStatus()
    {
        if ($this->status == self::STATUS_DISABLE) {
            throw new \LogicException('Status already disable');
        }

        $this->status = self::STATUS_DISABLE;

        return $this;
    }

}