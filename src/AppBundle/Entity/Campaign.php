<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 08.02.18
 * Time: 22:35
 */

namespace AppBundle\Entity;


use Gedmo\Timestampable\Traits\TimestampableEntity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;


/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CampaignRepository")
 * @ORM\Table(name="campaign")
 * @Serializer\ExclusionPolicy(value="all")
 */
class Campaign
{

    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose()
     *
     * @var int
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @Serializer\Expose()
     * @var User
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\CampaignType")
     * @Serializer\Expose()
     *
     * @var CampaignType
     */
    protected $type;

    /**
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Serializer\Expose()
     *
     * @var string
     */
    protected $name;

    /**
     * @ORM\Column(name="custom_setting", type="string", length=255, nullable=true)
     *
     * @var string
     */
    protected $customSetting;

    /**
     * Campaign constructor.
     * @param User $user
     * @param CampaignType $type
     * @param string $name
     * @param string $customSetting
     */
    public function __construct(User $user, CampaignType $type, $name, $customSetting = null)
    {
        $this->user = $user;
        $this->type = $type;
        $this->name = $name;
        $this->customSetting = $customSetting;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return CampaignType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getCustomSetting()
    {
        return $this->customSetting;
    }

    /**
     * @param CampaignType $type
     */
    public function setType(CampaignType $type)
    {
        $this->type = $type;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param string $customSetting
     */
    public function setCustomSetting($customSetting)
    {
        $this->customSetting = $customSetting;
    }







}