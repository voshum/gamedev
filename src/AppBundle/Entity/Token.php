<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 11.02.18
 * Time: 15:09
 */

namespace AppBundle\Entity;


class Token
{

    /**
     * @var string
     */
    protected $key;

    /**
     * @var string
     */
    protected $username;

    /**
     * @var int
     */
    protected $expiredAt;

    /**
     * Token constructor.
     * @param string $key
     * @param string $username
     * @param int $expiredAt
     */
    public function __construct($key, $username, $expiredAt)
    {
        $this->key = $key;
        $this->username = $username;
        $this->expiredAt = $expiredAt;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return int
     */
    public function getExpiredAt()
    {
        return $this->expiredAt;
    }


}