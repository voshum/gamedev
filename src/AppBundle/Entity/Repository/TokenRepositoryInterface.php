<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 11.02.18
 * Time: 15:07
 */

namespace AppBundle\Entity\Repository;


use AppBundle\Entity\Token;

interface TokenRepositoryInterface
{

    /**
     * @param string $apiKey
     * @return Token
     */
    public function getByApiKey($apiKey);


}