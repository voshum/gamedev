<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 08.02.18
 * Time: 22:51
 */

namespace AppBundle\Entity\Repository;


use AppBundle\Entity\Campaign;

interface CampaignRepositoryInterface
{

    /**
     * @param Campaign $campaign
     * @return Campaign
     */
    public function add(Campaign $campaign);

    /**
     * @param $limit
     * @param $offset
     * @return Campaign[]
     */
    public function getCampaigns($limit, $offset);

    /**
     * @param $id
     * @return Campaign|null
     */
    public function getById($id);

    /**
     * @param Campaign $campaign
     * @return mixed
     */
    public function remove(Campaign $campaign);

    /**
     * @param Campaign $campaign
     * @return mixed
     */
    public function update(Campaign $campaign);


}