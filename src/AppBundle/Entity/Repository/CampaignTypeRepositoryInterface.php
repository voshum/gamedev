<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 10.02.18
 * Time: 15:36
 */

namespace AppBundle\Entity\Repository;


use AppBundle\Entity\CampaignType;

interface CampaignTypeRepositoryInterface
{

    /**
     * @param CampaignType $campaignType
     * @return mixed
     */
    public function add(CampaignType $campaignType);

    /**
     * @param $id
     * @return CampaignType|null
     */
    public function getById($id);

}