<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 08.02.18
 * Time: 22:50
 */

namespace AppBundle\Entity\Repository;


use AppBundle\Entity\User;

interface UserRepositoryInterface
{

    /**
     * @param User $user
     * @return User
     */
    public function add(User $user);

    /**
     * @param $limit
     * @param $offset
     * @return User[]
     */
    public function getUsers($limit, $offset);

    /**
     * @param $id
     * @return User|null
     */
    public function getById($id);

    /**
     * @param $email
     * @return User|null
     */
    public function getByEmail($email);


}