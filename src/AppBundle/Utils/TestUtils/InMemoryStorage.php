<?php

namespace AppBundle\Utils\TestUtils;

class InMemoryStorage implements \Iterator
{

    protected $items = [];

    protected $pk = 1;

    /**
     * @var string
     */
    protected $pkProperty;

    /**
     * InMemoryStorage constructor.
     */
    public function __construct($pkProperty = 'id')
    {
        $this->pkProperty = $pkProperty;
    }


    public function current()
    {
        return current($this->items);
    }

    public function next()
    {
        next($this->items);
    }

    public function key()
    {
        return key($this->items);
    }

    public function valid()
    {
        return isset($this->items[$this->key()]);
    }

    public function rewind()
    {
        reset($this->items);
    }

    public function add($item)
    {
        $getPkValue = 'get'.ucfirst($this->pkProperty);


        if ($item->{$getPkValue}() == null) {
            $this->setProtectedPropertyValue($item, $this->pkProperty, $this->pk);
            $this->items[$this->pk] = $item;
            $this->pk++;
        } else {
            $this->items[$item->{$getPkValue}()] = $item;
        }
    }

    /**
     * @param $item
     */
    public function addWithoutPk($item)
    {
        $this->items[] = $item;
    }

    public function all()
    {
        return $this->items;
    }

    public function remove($pk)
    {
        if ($this->valid()) {
            unset($this->items[$pk]);
        }
    }

    public function getByPk($pk)
    {
        return isset($this->items[$pk]) ? $this->items[$pk] : null;
    }

    public function delete($pk)
    {
        unset($this->items[$pk]);
    }


    protected function setProtectedPropertyValue($object, $property, $value)
    {
        $reflectionObject = new \ReflectionObject($object);
        $reflectionProperty = $reflectionObject->getProperty($property);
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($object, $value);
    }

}