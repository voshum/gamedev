<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 10.02.18
 * Time: 12:56
 */

namespace AppBundle\Utils\TestUtils;


use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class Md5PasswordEncoder implements UserPasswordEncoderInterface
{

    /**
     * @param UserInterface $user
     * @param string $plainPassword
     * @return string
     */
    public function encodePassword(UserInterface $user, $plainPassword)
    {
        return md5($plainPassword);
    }

    /**
     * @param UserInterface $user
     * @param string $raw
     * @return bool
     */
    public function isPasswordValid(UserInterface $user, $raw)
    {
        return $user->getPassword() == md5($raw);
    }


}