<?php

namespace AppBundle\Utils\TestUtils;

use Symfony\Component\PropertyAccess\PropertyAccess;

class InMemoryRepository
{

    protected $storage;

    /**
     * InMemoryRepository constructor.
     *
     */
    public function __construct()
    {
        $this->storage = new InMemoryStorage($this->getPkProperty());
    }


    public function clear()
    {
        $this->storage = new InMemoryStorage($this->getPkProperty());
    }

    /**
     * @param $attribute
     * @param $value
     * @return null|object
     */
    protected function getOneBy($attribute, $value)
    {
        $accessor = PropertyAccess::createPropertyAccessor();

        foreach ($this->storage->all() as $item) {
            if ($accessor->getValue($item, $attribute) == $value) {
                return $item;
            }
        }

        return null;
    }

    public function getPkProperty()
    {
        return 'id';
    }

}