<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 10.02.18
 * Time: 17:05
 */

namespace AppBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class CampaignType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('campaign_type_id', TextType::class, [
                'constraints' => [
                    new NotBlank()
                ],
                'property_path' => 'campaignTypeId',
            ])
            ->add('name', TextType::class, [
                'constraints' => [
                    new NotBlank()
                ],
            ])
            ->add('custom_setting', TextType::class, [
                'property_path' => 'customSetting',
            ]);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('csrf_protection', false);
    }


}