<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 10.02.18
 * Time: 17:08
 */

namespace AppBundle\Form\Data;


class CampaignData
{
    /**
     * @var int
     */
    public $userId;

    /**
     * @var int
     */
    public $campaignTypeId;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $customSetting;


}