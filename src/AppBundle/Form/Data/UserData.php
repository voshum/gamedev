<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 10.02.18
 * Time: 16:22
 */

namespace AppBundle\Form\Data;


class UserData
{

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $plainPassword;

    /**
     * UserData constructor.
     * @param string $name
     * @param string $email
     * @param string $plainPassword
     */
    public function __construct($name = null, $email = null, $plainPassword = null)
    {
        $this->name = $name;
        $this->email = $email;
        $this->plainPassword = $plainPassword;
    }


}