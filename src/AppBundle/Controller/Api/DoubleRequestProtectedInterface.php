<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 12.02.18
 * Time: 22:57
 */

namespace AppBundle\Controller\Api;


interface DoubleRequestProtectedInterface
{

    /**
     * @return array
     */
    public function getProtectedMethods();

}