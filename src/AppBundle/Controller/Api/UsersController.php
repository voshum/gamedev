<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 10.02.18
 * Time: 13:02
 */

namespace AppBundle\Controller\Api;


use AppBundle\Form\Data\UserData;
use AppBundle\Form\Type\UserType;
use AppBundle\Service\UserService;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\Request;
use Swagger\Annotations as SWG;


class UsersController extends FOSRestController implements DoubleRequestProtectedInterface
{

    /**
     * @var UserService
     */
    protected $userService;

    /**
     * UsersController constructor.
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Список методов защищенных от множественного вызова
     * @return array
     */
    public function getProtectedMethods()
    {
        return [
            'postUsersAction',
        ];
    }


    /**
     * @SWG\Response(
     *     response=200,
     *     description="Список пользователей",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=AppBundle\Entity\User::class)
     *     )
     * )
     * @SWG\Parameter(
     *     name="apikey",
     *     in="query",
     *     type="string",
     *     description="Авторизационный ключ",
     *     required=true
     * )
     * @SWG\Tag(name="Пользователи")
     */
    public function getUsersAction(Request $request)
    {
        $users = $this->userService->getUsers();

        $view = $this->view($users);

        return $this->handleView($view);
    }


    /**
     * @SWG\Response(
     *     response=200,
     *     description="Создание пользователя",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=AppBundle\Entity\User::class)
     *     )
     * )
     * @SWG\Parameter(
     *     name="apikey",
     *     in="query",
     *     type="string",
     *     description="Авторизационный ключ",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     type="string",
     *     description="Данные пользователя",
     *     required=true,
     *     @SWG\Schema(ref="#/definitions/User")
     * )
     * @SWG\Tag(name="Пользователи")
     */
    public function postUsersAction(Request $request)
    {
        $formData = new UserData();
        $form = $this->createForm(UserType::class, $formData);

        $form->submit($request->request->all());

        if ($form->isValid()) {

            $user = $this->userService->createUser(
                $formData->name, $formData->email, $formData->plainPassword
            );

            return $this->handleView(
                $this->view($user)
            );

        }

        return $this->handleView(
            $this->view($form->getErrors())
        );
    }



}
