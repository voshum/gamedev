<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 10.02.18
 * Time: 16:52
 */

namespace AppBundle\Controller\Api;


use AppBundle\Exception\ObjectNotFoundException;
use AppBundle\Form\Data\CampaignData;
use AppBundle\Form\Type\CampaignType;
use AppBundle\Service\CampaignService;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;


class CampaignsController extends FOSRestController implements DoubleRequestProtectedInterface
{
    /**
     * @var CampaignService
     */
    protected $campaignService;

    /**
     * CampaignsController constructor.
     * @param CampaignService $campaignService
     */
    public function __construct(CampaignService $campaignService)
    {
        $this->campaignService = $campaignService;
    }

    /**
     * Список методов защищенных от множественного вызова
     * @return array
     */
    public function getProtectedMethods()
    {
        return [
            'deleteCampaignAction',
            'putCampaignAction',
            'postUsersCampaignsAction',
        ];
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Список кампаний",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=AppBundle\Entity\Campaign::class)
     *     )
     * )
     * @SWG\Parameter(
     *     name="apikey",
     *     in="query",
     *     type="string",
     *     description="Авторизационный ключ",
     *     required=true
     * )
     * @SWG\Tag(name="Кампания")
     */
    public function getCampaignsAction(Request $request)
    {
        $view = $this->view(
            $this->campaignService->getCampaigns()
        );

        return $this->handleView($view);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Информация по кампании",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=AppBundle\Entity\Campaign::class)
     *     )
     * )
     * @SWG\Parameter(
     *     name="apikey",
     *     in="query",
     *     type="string",
     *     description="Авторизационный ключ",
     *     required=true
     * )
     * @SWG\Tag(name="Кампания")
     */
    public function getCampaignAction(Request $request, $id)
    {
        try {

            $view = $this->view($this->campaignService->getCampaign($id));

            return $this->handleView($view);

        } catch (ObjectNotFoundException $exception) {
            throw $this->createNotFoundException($exception->getMessage());
        }
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Удаление кампании",
     *     @SWG\Schema(
     *         type="number",
     *     )
     * )
     * @SWG\Parameter(
     *     name="apikey",
     *     in="query",
     *     type="string",
     *     description="Авторизационный ключ",
     *     required=true
     * )
     * @SWG\Tag(name="Кампания")
     */
    public function deleteCampaignAction(Request $request, $id)
    {
        try {

            $this->campaignService->deleteCampaign($id);

            return $this->handleView(
                $this->view([$id])
            );

        } catch (ObjectNotFoundException $exception) {
            throw $this->createNotFoundException($exception->getMessage());
        }
    }


    /**
     * @SWG\Response(
     *     response=200,
     *     description="Редактирование кампании",
     *     @SWG\Schema(
     *         type="object",
     *         @Model(type=AppBundle\Entity\Campaign::class)
     *     )
     * )
     * @SWG\Parameter(
     *     name="apikey",
     *     in="query",
     *     type="string",
     *     description="Авторизационный ключ",
     *     required=true
     * )
     * @SWG\Tag(name="Кампания")
     */
    public function putCampaignAction(Request $request, $id)
    {
        $formData = new CampaignData();
        $form = $this->createForm(CampaignType::class, $formData);

        $form->submit($request->request->all());

        if ($form->isValid()) {

            try {

                $campaign = $this->campaignService->updateCampaign(
                    $id, $formData->campaignTypeId, $formData->name, $formData->customSetting
                );

                $view = $this->view($campaign);

                return $this->handleView($view);

            } catch (ObjectNotFoundException $e) {
                throw $this->createNotFoundException($e->getMessage());
            }

        }

        $view = $this->view($form->getErrors());

        return $this->handleView($view);
    }


    /**
     * @SWG\Response(
     *     response=200,
     *     description="Создание кампании",
     *     @SWG\Schema(
     *         type="object",
     *         @Model(type=AppBundle\Entity\Campaign::class)
     *     )
     * )
     * @SWG\Parameter(
     *     name="apikey",
     *     in="query",
     *     type="string",
     *     description="Авторизационный ключ",
     *     required=true
     * )
     * @SWG\Tag(name="Кампания")
     */
    public function postUsersCampaignsAction(Request $request, $userId)
    {
        $formData = new CampaignData();
        $form = $this->createForm(CampaignType::class, $formData);

        $form->submit($request->request->all());

        if ($form->isValid()) {

            try {

                $campaign = $this->campaignService->createCampaign(
                    $userId, $formData->campaignTypeId, $formData->name, $formData->customSetting
                );

                $view = $this->view($campaign);

                return $this->handleView($view);


            } catch (ObjectNotFoundException $e) {
                throw $this->createNotFoundException($e->getMessage());
            }

        }

        $view = $this->view($form->getErrors());

        return $this->handleView($view);
    }

}