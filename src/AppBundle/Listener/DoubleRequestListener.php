<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 12.02.18
 * Time: 22:45
 */

namespace AppBundle\Listener;


use AppBundle\Controller\Api\DoubleRequestProtectedInterface;
use AppBundle\Service\DoubleRequestProtector;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class DoubleRequestListener
{

    /**
     * @var DoubleRequestProtector
     */
    protected $protector;

    /**
     * DoubleRequestListener constructor.
     * @param DoubleRequestProtector $protector
     */
    public function __construct(DoubleRequestProtector $protector)
    {
        $this->protector = $protector;
    }


    public function onKernelController(FilterControllerEvent $event)
    {
        if (is_array($event->getController())) {
            $controller = $event->getController()[0];
            $action = $event->getController()[1];

        } else {
            $controller = $event->getController();
            $action = null;
        }

        if ($controller instanceof DoubleRequestProtectedInterface) {

            if (in_array($action, $controller->getProtectedMethods())) {

                if ($this->protector->requestIsAllow($event->getRequest())) {
                    $this->protector->markRequest($event->getRequest());
                } else {
                    throw new BadRequestHttpException('Detect double request. Wait 1 minutes...');
                }

            }

        }

    }


}