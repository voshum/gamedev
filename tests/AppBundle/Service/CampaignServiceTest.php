<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 12.02.18
 * Time: 21:57
 */

namespace AppBundle\Service;


use AppBundle\Entity\Campaign;
use AppBundle\Entity\CampaignType;
use AppBundle\Entity\Repository\CampaignTypeRepositoryInterface;
use AppBundle\Entity\User;
use AppBundle\Repository\InMemoryCampaignRepository;
use AppBundle\Repository\InMemoryCampaignTypeRepository;
use AppBundle\Repository\InMemoryUserRepository;
use PHPUnit\Framework\TestCase;

class CampaignServiceTest extends TestCase
{
    /**
     * @var CampaignService
     */
    protected $service;

    /**
     * @var InMemoryUserRepository
     */
    protected $userRepository;

    /**
     * @var InMemoryCampaignRepository
     */
    protected $campaignRepository;

    /**
     * @var CampaignTypeRepositoryInterface
     */
    protected $campaignTypeRepository;



    protected function setUp()
    {
        $this->userRepository = new InMemoryUserRepository();
        $this->campaignRepository = new InMemoryCampaignRepository();
        $this->campaignTypeRepository = new InMemoryCampaignTypeRepository();

        $this->service = new CampaignService(
            $this->userRepository,
            $this->campaignRepository,
            $this->campaignTypeRepository
        );

        $this->userRepository->add(
            new User('Alex', 'alex@gmail.com')
        );

        $this->campaignTypeRepository->add(
            new CampaignType('Type 1')
        );

        $this->campaignTypeRepository->add(
            new CampaignType('Type 2')
        );

    }


    public function testCreateCampaign()
    {
        $campaign = $this->service->createCampaign(1, 1, 'Кампания 1', null);

        $this->assertInstanceOf(Campaign::class, $campaign);
        $this->assertEquals('Кампания 1', $campaign->getName());
        $this->assertEquals(1, $campaign->getId());
        $this->assertEquals(1, $campaign->getUser()->getId());
        $this->assertEquals(1, $campaign->getType()->getId());
        $this->assertNull($campaign->getCustomSetting());
    }

    public function testUpdateCampaign()
    {
        $campaign = $this->service->createCampaign(1, 1, 'Кампания 1', null);


        $this->assertInstanceOf(Campaign::class, $campaign);
        $this->assertEquals('Кампания 1', $campaign->getName());
        $this->assertEquals(1, $campaign->getId());
        $this->assertEquals(1, $campaign->getUser()->getId());
        $this->assertEquals(1, $campaign->getType()->getId());
        $this->assertNull($campaign->getCustomSetting());

        $this->service->updateCampaign(
            1, 2, 'Кампания 2', 'cs'
        );

        $this->assertInstanceOf(Campaign::class, $campaign);
        $this->assertEquals('Кампания 2', $campaign->getName());
        $this->assertEquals(1, $campaign->getId());
        $this->assertEquals(1, $campaign->getUser()->getId());
        $this->assertEquals(2, $campaign->getType()->getId());
        $this->assertEquals('cs', $campaign->getCustomSetting());
    }


    public function testGetCampaign()
    {
        $user = $this->createMock(User::class);
        $user->method('getId')->willReturn(1);

        $type = $this->createMock(CampaignType::class);
        $type->method('getId')->willReturn(3);

        $this->campaignRepository->add(
            new Campaign($user, $type, 'Камарания 3')
        );

        $campaign = $this->service->getCampaign(1);

        $this->assertInstanceOf(Campaign::class, $campaign);
        $this->assertEquals('Камарания 3', $campaign->getName());
        $this->assertEquals(1, $campaign->getId());
        $this->assertEquals(1, $campaign->getUser()->getId());
        $this->assertEquals(3, $campaign->getType()->getId());
    }


    public function testDeleteCampaign()
    {
        $user = $this->createMock(User::class);
        $user->method('getId')->willReturn(1);

        $type = $this->createMock(CampaignType::class);
        $type->method('getId')->willReturn(3);

        $this->campaignRepository->add(
            new Campaign($user, $type, 'Камарания 3')
        );

        $campaign = $this->service->getCampaign(1);

        $this->assertInstanceOf(Campaign::class, $campaign);

        $this->service->deleteCampaign(1);

        $campaign = $this->service->getCampaign(1);

        $this->assertNull($campaign);
    }





}