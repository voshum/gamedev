<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 10.02.18
 * Time: 12:20
 */

namespace AppBundle\Service;


use AppBundle\Entity\Repository\UserRepositoryInterface;
use AppBundle\Entity\User;
use AppBundle\Repository\InMemoryUserRepository;
use AppBundle\Utils\InMemoryRepository;
use AppBundle\Utils\TestUtils\Md5PasswordEncoder;
use PHPUnit\Framework\TestCase;

class UserServiceTest extends TestCase
{
    /**
     * @var UserService
     */
    protected $service;

    /**
     * @var UserRepositoryInterface
     */
    protected $userRepository;


    protected function setUp()
    {
        $this->userRepository = new InMemoryUserRepository();

        $this->service = new UserService($this->userRepository, new Md5PasswordEncoder());

    }


    public function testCreateUser()
    {
        $user = $this->service->createUser('Alex', 'alex@gmail.com', 'planpassword');

        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals('Alex', $user->getName());

        $this->assertNotNull($user->getPassword());
        $this->assertNotEquals('', $user->getPassword());
        $this->assertNotEquals('planpassword', $user->getPassword());

    }


}