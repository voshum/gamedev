<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180211170046 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE user_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE campaign_type_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE campaign_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE "user" (
          id INT NOT NULL, 
          name VARCHAR(255) NOT NULL, 
          email VARCHAR(255) NOT NULL, 
          password VARCHAR(255) NOT NULL, 
          salt VARCHAR(255) NOT NULL, 
          roles JSON NOT NULL, 
          created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, 
          updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, 
          PRIMARY KEY(id)
        )');
        $this->addSql('CREATE TABLE campaign_type (
          id INT NOT NULL, 
          name VARCHAR(255) NOT NULL, 
          status VARCHAR(255) NOT NULL, 
          created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, 
          updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, 
          PRIMARY KEY(id)
        )');
        $this->addSql('CREATE TABLE campaign (
          id INT NOT NULL, 
          user_id INT DEFAULT NULL, 
          type_id INT DEFAULT NULL, 
          name VARCHAR(255) NOT NULL, 
          custom_setting VARCHAR(255) DEFAULT NULL, 
          created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, 
          updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, 
          PRIMARY KEY(id)
        )');
        $this->addSql('CREATE INDEX IDX_1F1512DDA76ED395 ON campaign (user_id)');
        $this->addSql('CREATE INDEX IDX_1F1512DDC54C8C93 ON campaign (type_id)');
        $this->addSql('ALTER TABLE 
          campaign 
        ADD 
          CONSTRAINT FK_1F1512DDA76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE 
          campaign 
        ADD 
          CONSTRAINT FK_1F1512DDC54C8C93 FOREIGN KEY (type_id) REFERENCES campaign_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE campaign DROP CONSTRAINT FK_1F1512DDA76ED395');
        $this->addSql('ALTER TABLE campaign DROP CONSTRAINT FK_1F1512DDC54C8C93');
        $this->addSql('DROP SEQUENCE user_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE campaign_type_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE campaign_id_seq CASCADE');
        $this->addSql('DROP TABLE "user"');
        $this->addSql('DROP TABLE campaign_type');
        $this->addSql('DROP TABLE campaign');
    }
}
